﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Data
{
    public class PromoCodeDbInitializer : IDbInitializer
    {
        private readonly PromoCodeDbContext _dataContext;

        public PromoCodeDbInitializer(PromoCodeDbContext dataContext)
        {
            _dataContext = dataContext;
        }

        public void InitializeDb()
        {
            //_dataContext.Database.EnsureDeleted();
            if (_dataContext.Database.EnsureCreated())
            {
                _dataContext.Set<Employee>().AddRange(FakeDataFactory.Employees);
                _dataContext.AddRange(FakeDataFactory.Preferences);
                _dataContext.Set<Customer>().AddRange(FakeDataFactory.Customers);
                _dataContext.SaveChanges();

                var preferences = _dataContext.Set<Preference>();
                var roles = _dataContext.Set<Role>();

                preferences.AddRange(FakeDataFactory.Preferences.Where(p => !preferences.Any(x => x.Id == p.Id)));
                roles.AddRange(FakeDataFactory.Roles.Where(r => !roles.Any(x => x.Id == r.Id)));
                _dataContext.SaveChanges();
            }

        }
    }
}
