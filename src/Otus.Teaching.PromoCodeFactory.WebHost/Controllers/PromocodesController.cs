﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Промокоды
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class PromocodesController
        : ControllerBase
    {
        private readonly IRepository<PromoCode> _promoCodesRepository;
        private readonly IRepository<Customer> _customerRepository;
        public PromocodesController(IRepository<Customer> customerRepository, IRepository<PromoCode> promoCodesRepository)
        {
            _promoCodesRepository = promoCodesRepository;
            _customerRepository = customerRepository;
        }

        /// <summary>
        /// Получить все промокоды
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<List<PromoCodeShortResponse>>> GetPromocodesAsync()
        {
            var preferences = await _promoCodesRepository.GetAllAsync();

            var response = preferences.Select(x => new PromoCodeShortResponse()
            {
                Id = x.Id,
                Code = x.Code,
                BeginDate = x.BeginDate.ToString("yyyy-MM-dd"),
                EndDate = x.EndDate.ToString("yyyy-MM-dd"),
                PartnerName = x.PartnerName,
                ServiceInfo = x.ServiceInfo
            }).ToList();

            return Ok(response);
        }
        
        /// <summary>
        /// Создать промокод и выдать его клиентам с указанным предпочтением
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> GivePromoCodesToCustomersWithPreferenceAsync(GivePromoCodeRequest request)
        {
            var customers = (await _customerRepository.GetAllAsync()).ToList();

            foreach(var customer in customers.Where(c => c.CustomerPreferences.Where(p => p.Preference.Name == request.Preference).Any()))
            {
                await _promoCodesRepository.CreateAsync(new PromoCode
                {
                    Id = Guid.NewGuid(),
                    BeginDate = DateTime.UtcNow,
                    EndDate = DateTime.UtcNow.AddDays(30),
                    PartnerName = request.PartnerName,
                    Code = request.PromoCode,
                    ServiceInfo = request.ServiceInfo,
                    CustomerId = customer.Id
                });
            }

            return Ok();
        }
    }
}