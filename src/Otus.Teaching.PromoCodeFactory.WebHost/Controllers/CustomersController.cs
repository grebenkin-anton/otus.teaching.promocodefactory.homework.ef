﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Клиенты
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class CustomersController
        : ControllerBase
    {
        private readonly IRepository<Customer> _customerRepository;
        private readonly IRepository<PromoCode> _promoCodeRepository;
        private readonly IRepository<Preference> _preferenceRepository;

        public CustomersController(IRepository<Customer> customerRepository, IRepository<PromoCode> promoCodeRepository,
            IRepository<Preference> preferenceRepository)
        {
            _customerRepository = customerRepository;
            _promoCodeRepository = promoCodeRepository;
            _preferenceRepository = preferenceRepository;
        }

        /// <summary>
        /// Получить данные всех клиентов
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<CustomerShortResponse>> GetCustomersAsync()
        {
            var customers = await _customerRepository.GetAllAsync();

            var response = customers.Select(x => new CustomerShortResponse()
            {
                Id = x.Id,
                Email = x.Email,
                FirstName = x.FirstName,
                LastName = x.LastName
            }).ToList();

            return Ok(response);
        }

        /// <summary>
        /// Получить данные клиента по Id
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<CustomerResponse>> GetCustomerAsync(Guid id)
        {
            var customer = await _customerRepository.GetByIdAsync(id);

            if (customer == null)
                return NotFound();

            var response = new CustomerResponse 
            {
                Email = customer.Email,
                FirstName = customer.FirstName,
                Id = customer.Id,
                LastName = customer.LastName,
                Preferences = new List<PreferenceResponse>(),
                PromoCodes = new List<PromoCodeShortResponse>()
            };

            if (customer.CustomerPreferences != null)
            {
                response.Preferences = new List<PreferenceResponse>();

                foreach (var item in customer.CustomerPreferences)
                {
                    response.Preferences.Add(new PreferenceResponse
                    {
                        Id = item.Preference.Id,
                        Name = item.Preference.Name
                    });
                }
            }

            if (customer.PromoCodes != null)
            {
                response.PromoCodes = new List<PromoCodeShortResponse>();

                foreach (var item in customer.PromoCodes)
                {
                    response.PromoCodes.Add(new PromoCodeShortResponse
                    {
                        Id = item.Id,
                        BeginDate = item.BeginDate.ToString(),
                        Code = item.Code,
                        EndDate = item.EndDate.ToString(),
                        PartnerName = item.PartnerName,
                        ServiceInfo = item.ServiceInfo            
                    });
                }
            }
 
            return Ok(response);
        }

        /// <summary>
        /// Добавить клиента
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult<Guid>> CreateCustomerAsync([FromBody] CreateOrEditCustomerRequest request)
        {
            Customer customer = new Customer
            {
                FirstName = request.FirstName,
                LastName = request.LastName,
                Email = request.Email,
                Id = Guid.NewGuid()
            };

            if (request.PreferenceIds != null)
            {
                foreach(var item in request.PreferenceIds)
                {
                    customer.CustomerPreferences.Add(new CustomerPreference
                    {
                        Id = Guid.NewGuid(),
                        CustomerId = customer.Id,
                        PreferenceId = item
                    });
                }
                
            }

            await _customerRepository.CreateAsync(customer);

            return Ok(customer.Id);
        }


        /// <summary>
        /// Редактировать клиента
        /// </summary>
        /// <returns></returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> EditCustomersAsync(Guid id, CreateOrEditCustomerRequest request)
        {
            var customer = await _customerRepository.GetByIdAsync(id);

            if (customer == null)
                return NotFound();

            customer.FirstName = request.FirstName;
            customer.LastName = request.LastName;
            customer.Email = request.Email;
            
            if (request.PreferenceIds != null)
            {
                customer.CustomerPreferences = new List<CustomerPreference>();
                foreach (var item in request.PreferenceIds)
                {
                    customer.CustomerPreferences.Add(new CustomerPreference
                    {
                        Id = Guid.NewGuid(),
                        CustomerId = customer.Id,
                        PreferenceId = item
                    });
                }

            }

            await _customerRepository.UpdateAsync(customer);

            return NoContent();
        }

        /// <summary>
        /// Удалить клиента
        /// </summary>
        /// <returns></returns>
        [HttpDelete]
        public async Task<IActionResult> DeleteCustomer(Guid id)
        {
            var customer = await _customerRepository.GetByIdAsync(id);

            if (customer == null)
                return NotFound();

            await _customerRepository.DeleteAsync(customer);

            return NoContent();
        }
    }
}